package org.example;

import java.sql.*;

public class App {
    public static void main( String[] args )    {
        try{
            Connection connection = DriverManager.getConnection(
                "jdbc:mariadb://localhost:3306/classicmodels",
                "root", "1"
            );
        /* 2 */
//            selectOfficesHasCountryLikeUSA(connection);
        /* 3 */
//            selectEmployeesAndFullName(connection);
        /* 4 */
//            selectEmployeesAndOfficesCityState(connection);
        /* 5 */
//            selectNoneOfEmployeesOffices(connection);
        /* 6 */
//            selectCountOfEmployees(connection);
        /* 7 */
//            selectCustomersHaveUpTo3Payment(connection);
        /* 8 */
//            selectTop10CustomersHaveEarliestPayment(connection);
        /* 29 */
//            select1stOfficeHaveMostEmployeeHaveCustomer(connection);
        /* 30 */
//            select2CustomerHaveLimitCreditLower20000(connection);
        /* 31 */
//            select2MaxOfQuantityStockProducts(connection);
        /* 32 */
//            selectTop10ProductCheapestAndMostPopular(connection);
        /* 33 */
//            selectTop5ProductsHaveMaxCancellation(connection);
        /* 34 */
//            selectTop5EarliestShippedIn2004Products(connection);
        /* 35 */
//            selectSumPaymentOfEachCustomerIn2004(connection);
        /* 36 */
//            selectTop2ReportedToEmployees(connection);

            connection.close();
        }catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    /* 36 */
    private static void selectTop2ReportedToEmployees(Connection connection) throws SQLException {
        String select = "SELECT * FROM employees e \n" +
                "WHERE NOT isNULL(reportsTo)\n" +
                "ORDER BY reportsTo DESC \n" +
                "LIMIT 2";
        Statement statement = connection.createStatement();
        ResultSet resultSet;

        statement.execute(select);
        resultSet = statement.getResultSet()    ;
        while (resultSet.next()) {
            String row = String.format("%d | %s | %s | %s | %s | %s | %d | %s",
                    resultSet.getInt(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4),
                    resultSet.getString(5),
                    resultSet.getString(6),
                    resultSet.getInt(7),
                    resultSet.getString(8)
            );

            System.out.println(row);
        }

    }


    /* 35 */
    private static void selectSumPaymentOfEachCustomerIn2004(Connection connection) throws SQLException {
        String select = "SELECT  p.customerNumber , sum(amount) \n" +
                "FROM payments p \n" +
                "WHERE paymentDate  LIKE '2004%'\n" +
                "GROUP BY customerNumber";
        Statement statement = connection.createStatement();
        ResultSet resultSet ;

        statement.execute(select);
        resultSet = statement.getResultSet();
        while (resultSet.next()) {
            String row = String.format("%3d | %10d",
                    resultSet.getInt(1),
                    resultSet.getInt(2)
            );

            System.out.println(row);
        }
    }

    /* 34 */
    private static void selectTop5EarliestShippedIn2004Products(Connection connection) throws SQLException {
        String select = "SELECT shippedDate , p.* \n" +
                "FROM orders o  \tJOIN orderdetails o2  \tON o.orderNumber = o2.orderNumber \n" +
                "\t\t\t\tJOIN products p  \t\tON  p.productCode = o2.productCode  \n" +
                "WHERE shippedDate LIKE '2004%'\n" +
                "ORDER BY shippedDate \n" +
                "LIMIT 5 ";
        Statement statement = connection.createStatement();
        ResultSet resultSet;

        statement.execute(select);
        resultSet = statement.getResultSet();
        while (resultSet.next()) {
            String row = String.format("%tF | %s | %40s | %15s | %s | %30s | %s | %d | %d | %d",
                    resultSet.getDate(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4),
                    resultSet.getString(5),
                    resultSet.getString(6),
                    resultSet.getString(7),
                    resultSet.getInt(8),
                    resultSet.getInt(9),
                    resultSet.getInt(10)
            );

            System.out.println(row);
        }

    }

    /* 33 */
    private static void selectTop5ProductsHaveMaxCancellation(Connection connection) throws SQLException {
        String select = "SELECT count(o2.productCode ) AS 'quantityOrderCancellation' , p.* \n" +
                "FROM orders o\tJOIN orderdetails o2 \tON o.orderNumber = o2.orderNumber \n" +
                "\t\t\t\tJOIN products p \t\tON o2.productCode = p.productCode \n" +
                "WHERE o.shippedDate IS NULL \n" +
                "GROUP BY o2.productCode \n" +
                "ORDER BY count(o2.productCode ) DESC \n" +
                "LIMIT 5 ";
        Statement statement = connection.createStatement();
        ResultSet resultSet;

        statement.execute(select);
        resultSet = statement.getResultSet();
        while (resultSet.next()) {
            String row = String.format("%d | %s | %30s | %15s | %s | %30s | %s | %d | %d | %d",
                    resultSet.getInt(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4),
                    resultSet.getString(5),
                    resultSet.getString(6),
                    resultSet.getString(7),
                    resultSet.getInt(8),
                    resultSet.getInt(9),
                    resultSet.getInt(10)
            );

            System.out.println(row);
        }
    }

    /* 32 */
    private static void select10ProductCheapestAndMostPopular(Connection connection) throws SQLException {
        String select = "SELECT p.productCode , p.productName, p.buyPrice  , count(p.productCode)  AS \"so lan mua\"\n" +
                "FROM products p JOIN orderdetails o ON o.productCode = p.productCode \n" +
                "\t\t\t\tJOIN orders o2 \t\tON o2.orderNumber = o.orderNumber \n" +
                "GROUP BY p.productCode \n" +
                "ORDER  BY p.buyPrice , count(p.productCode)\n" +
                "LIMIT 10 ";
        Statement statement = connection.createStatement();
        ResultSet resultSet ;

        statement.execute(select);
        resultSet = statement.getResultSet();
        while (resultSet.next()) {
            String row = String.format("%s | %40s | %d | %d",
                    resultSet.getString(1),
                    resultSet.getString(2),
                    resultSet.getInt(3),
                    resultSet.getInt(4)
            );

            System.out.println(row);
        }
    }

    /* 31 */
    private static void select2MaxOfQuantityStockProducts(Connection connection) throws SQLException {
        String select = "SELECT * \n" +
                "FROM products p \n" +
                "ORDER BY quantityInStock DESC\n" +
                "LIMIT 2";
        Statement statement = connection.createStatement();
        ResultSet resultSet;

        statement.execute(select);
        resultSet = statement.getResultSet();
        while (resultSet.next()) {
            String row = String.format("%s | %s | %s | %s | %s | %s | %d | %d | %d",
                    resultSet.getString(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4),
                    resultSet.getString(5),
                    resultSet.getString(6),
                    resultSet.getInt(7),
                    resultSet.getInt(8),
                    resultSet.getInt(9)
            );

            System.out.println(row);
        }
    }

    /* 30 */
    private static void select2CustomerHaveLimitCreditLower20000(Connection connection) throws SQLException {
        String select = "SELECT * \n" +
                "FROM customers c \n" +
                "WHERE creditLimit < 20000\n" +
                "LIMIT 2";
        Statement statement = connection.createStatement();
        ResultSet resultSet ;

        statement.execute(select);
        resultSet = statement.getResultSet();
        while (resultSet.next()) {
            String row = String.format("%d | %s | %s | %s | %s | %s | %s | %s | %s | %s | %s | %d | %d",
                    resultSet.getInt(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4),
                    resultSet.getString(5),
                    resultSet.getString(6),
                    resultSet.getString(7),
                    resultSet.getString(8),
                    resultSet.getString(9),
                    resultSet.getString(10),
                    resultSet.getString(11),
                    resultSet.getInt(12),
                    resultSet.getInt(13)
            );

            System.out.println(row);
        }

    }

    /* 29 */
    private static void select1stOfficeHaveMostEmployeeHaveCustomer(Connection connection) throws SQLException {
        String selectStr = "SELECT o.officeCode , o.city , count(DISTINCT e.employeeNumber) \"numOfEmployeesHaveCustomer\"\n" +
                "FROM  customers c \tJOIN employees e\tON c.salesRepEmployeeNumber = e.employeeNumber \n" +
                "\t\t\t\t\tJOIN offices o  \tON o.officeCode = e.officeCode \n" +
                "GROUP BY o.officeCode \n" +
                "ORDER BY count(DISTINCT e.employeeNumber) DESC  \n" +
                "LIMIT 1";
        Statement statement = connection.createStatement();
        ResultSet resultSet ;

        statement.execute(selectStr);
        resultSet = statement.getResultSet();
        while (resultSet.next()) {
            String row = String.format("%s | %s | %d",
                    resultSet.getString(1),
                    resultSet.getString(2),
                    resultSet.getInt(3));

            System.out.println(row);
        }
    }

    /* 8 */
    private static void selectTop10CustomersHaveEarliestPayment(Connection connection) throws SQLException {
        String selectStr = "SELECT DISTINCT c.*\n" +
                "FROM tbl1 t JOIN customers c  ON t.customerNumber = c.customerNumber  \n" +
                "LIMIT 10 ";
        Statement statement = connection.createStatement();
        ResultSet resultSet ;

        statement.execute(selectStr);
        resultSet = statement.getResultSet();
        while (resultSet.next()) {
            String row = String.format("%d | %35s | %10s | %10s  | %s | %s | %s | %s | %s | %s | %s | %d | %d",
                    resultSet.getInt(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4),
                    resultSet.getString(5),
                    resultSet.getString(6),
                    resultSet.getString(7),
                    resultSet.getString(8),
                    resultSet.getString(9),
                    resultSet.getString(10),
                    resultSet.getString(11),
                    resultSet.getInt(12),
                    resultSet.getInt(13)
            );

            System.out.println(row);
        }
    }

    /* 7 */
    private static void selectCustomersHaveUpTo3Payment(Connection connection) throws SQLException {
        String selectStr = "SELECT count(p.customerNumber) AS 'num of payment', c.*\n" +
                "FROM customers c\n" +
                "LEFT JOIN payments p \n" +
                "\tON (c.customerNumber = p.customerNumber)\n" +
                "GROUP BY c.customerNumber\n" +
                "HAVING count(p.customerNumber) < 4;";
        Statement selectStatement = connection.createStatement();
        ResultSet resultSet;

        selectStatement.execute(selectStr);
        resultSet = selectStatement.getResultSet();
        while (resultSet.next()) {
            String row = String.format("%1d | %3d | %35s | %20s | %15s | %12s | %30s | %30s | %15s | %5s | %6s | %10s | %5d | %7d",
                    resultSet.getInt(1),
                    resultSet.getInt(2),
                    resultSet.getString(3),
                    resultSet.getString(4),
                    resultSet.getString(5),
                    resultSet.getString(6),
                    resultSet.getString(7),
                    resultSet.getString(8),
                    resultSet.getString(9),
                    resultSet.getString(10),
                    resultSet.getString(11),
                    resultSet.getString(12),
                    resultSet.getInt(13),
                    resultSet.getInt(14)
            );

            System.out.println(row);
        }
    }


    /* 6 */
    private static void selectCountOfEmployees(Connection connection) throws SQLException {
        String selectStr = "SELECT count(e.employeeNumber) AS 'Số employee'\n" +
                "FROM employees e;";
        Statement selectStatement = connection.createStatement();
        ResultSet resultSet ;

        selectStatement.execute(selectStr);
        resultSet = selectStatement.getResultSet();
        while (resultSet.next()){
            String row = String.format("%d", resultSet.getInt("số employee"));

            System.out.println(row);
        }
    }

    /* 5 */
    private static void selectNoneOfEmployeesOffices(Connection connection) throws SQLException {
        String selectStr = "SELECT *\n" +
                "FROM offices o\n" +
                "WHERE NOT EXISTS (SELECT DISTINCT e.officeCode FROM employees e);";
        Statement selectStatement = connection.createStatement();
        ResultSet resultSet ;

        selectStatement.execute(selectStr);
        resultSet = selectStatement.getResultSet();
        while (resultSet.next()){
            String row = String.format("%s | %s | %s | %s | %s | %s | %s | %s | %s",
                    resultSet.getString(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4),
                    resultSet.getString(5),
                    resultSet.getString(6),
                    resultSet.getString(7),
                    resultSet.getString(8),
                    resultSet.getString(9)

            );

            System.out.println(row);
        }
    }

    /* 4 */
    private static void selectEmployeesAndOfficesCityState(Connection connection) throws SQLException {
        String selectStr = "SELECT e.*, o.city AS offices_city, o.state AS offices_state\n" +
                "FROM employees e , offices o\n" +
                "WHERE e.officeCode LIKE o.officeCode";
        Statement statement = connection.createStatement();
        ResultSet resultSet ;

        statement.execute(selectStr);
        resultSet = statement.getResultSet();
        while (resultSet.next()){  //
            String row = String.format("%4d | %10s | %10s | %6s | %40s | %1s | %5d | %15s | %15s | %2s",
                    resultSet.getInt("employeeNumber"),
                    resultSet.getString("lastName"),
                    resultSet.getString("firstName"),
                    resultSet.getString("extension"),
                    resultSet.getString("email"),
                    resultSet.getString("officeCode"),
                    resultSet.getInt("reportsTo"),
                    resultSet.getString("jobTitle"),
                    resultSet.getString("offices_city"),
                    resultSet.getString("offices_state")
            );

            System.out.println(row);
        }
    }

    /* 3 */
    private static void selectEmployeesAndFullName(Connection connection) throws SQLException {
        String selectStr = "SELECT e.employeeNumber , e.firstName, e.lastName , concat(e.firstName, ' ', e.lastName) AS fullName\n" +
                "FROM employees e";
        Statement selectStatement = connection.createStatement();
        ResultSet resultSet ;

        selectStatement.execute(selectStr);
        resultSet = selectStatement.getResultSet();
        while (resultSet.next()){
            String row = String.format("%4d | %10s | %10s | %10s",
                    resultSet.getInt(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4)
            );

            System.out.println(row);
        }
    }

    /* 2 */
    private static void selectOfficesHasCountryLikeUSA(Connection connection) throws SQLException {
        String selectStr = "SELECT * FROM offices WHERE country LIKE 'USA'";
        Statement selectStatement = connection.createStatement();
        ResultSet resultSet;

        selectStatement.execute(selectStr);
        resultSet = selectStatement.getResultSet();
        while (resultSet.next()){
            String row = String.format("%3s | %20s | %15s | %20s | %2s | %3s",
                    resultSet.getString("officeCode"),
                    resultSet.getString("city"),
                    resultSet.getString("phone"),
                    resultSet.getString("addressLine1"),
                    resultSet.getString("state"),
                    resultSet.getString(7)
            );

            System.out.println(row);
        }
    }
}
